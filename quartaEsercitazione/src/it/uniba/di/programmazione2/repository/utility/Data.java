package it.uniba.di.programmazione2.repository.utility;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Data {

	private Data() {};

	public static Date getDataAttuale() {

		SimpleDateFormat formatter= new SimpleDateFormat("yyyy");
		Date dataCreazione = new Date(System.currentTimeMillis());
		return dataCreazione;
	}
}