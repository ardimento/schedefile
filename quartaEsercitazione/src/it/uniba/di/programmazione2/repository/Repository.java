package it.uniba.di.programmazione2.repository;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

import it.uniba.di.programmazione2.repository.eccezione.*;

/**
 * Rappresenta un repository caratterizzato da un nome, da una ed una sola
 * scheda radice a partire dalla quale possono essere create nuove schede 
 * e nuovi file
 * @author pasqualeardimento
 *
 */
public class Repository {
	private String nome;
	private Node<Scheda> root;
	private Node<Scheda> risultatoRicerca;
	private Set<Scheda> risultatoAllSchede;

	/** 
	 * 
	 * @param nome Il nome del repository
	 */
	public Repository(String nome) {
		this.nome=nome;
		creaRoot();
	}

	/**
	 * Crea la scheda radice obbligatoriamente 
	 * presente in ogni Repository
	 */
	private void creaRoot() {
		root = new Node<>(new Scheda ("root"));
	}

	/**
	 * Crea una nuova scheda nella scheda radice del Repository
	 * @param nomeScheda Il nome della scheda da creare
	 * @throws RepositoryException
	 */
	public void creaScheda(String nomeScheda) throws RepositoryException {
		root.addChild(new Scheda (nomeScheda));
	}

	/**
	 * Crea una nuova scheda in una scheda già presente nel Repository
	 * @param nomeScheda Il nome della scheda da creare
	 * @param newNomeScheda Il nome della scheda in cui creare la scheda
	 * @throws RepositoryException
	 */

	public void creaSchedainScheda(String nomeScheda, String newNomeScheda) throws RepositoryException {
		Node<Scheda> schedaX = new Node<>(new Scheda(nomeScheda));
		Node<Scheda> nodo = searchInOrder(root, schedaX);
		if (nodo.equals(schedaX)) {
			System.out.println("\n\nTROVATO in CERCA SCHEDA\n\n");
			nodo.addChild(new Scheda (newNomeScheda));
		}
	}

	/**
	 * 
	 * @param node Il nodo a partire dal quale eseguire la ricerca
	 * @param nodeToSearch Il nodo da ricercare
	 * @return Restituisce il nodo trovato
	 */
	private Node<Scheda> searchInOrder(Node<Scheda> node, Node<Scheda> nodeToSearch) {
		if (node == null) {
			System.out.println("node is null");
		}
		Set<Node<Scheda>> nodi = node.getChildren();
		Iterator<Node<Scheda>> iteratore = nodi.iterator();
		while (iteratore.hasNext())
			searchInOrder(iteratore.next(), nodeToSearch);
		System.out.printf("%s ", node.getData().toString());
		if (node.equals(nodeToSearch))  {
			System.out.printf("TROVATO");
			risultatoRicerca = node;
		}
		return risultatoRicerca;
	}
	/**
	 * Rimuove una scheda, qualora presente
	 * @param nomeScheda Il nome della scheda rimuovere
	 * @throws RepositoryException
	 */

	public void rimuoviScheda(String nomeScheda) throws RepositoryException{
		Node<Scheda> schedaX = new Node<>(new Scheda(nomeScheda));
		Node<Scheda> nodo = searchInOrder(root, schedaX);
		if (nodo.equals(root))
			root.deleteChild(new Scheda(nomeScheda));
		else
			if (nodo.equals(schedaX)) {
				//System.out.println("\n\nTROVATO in CERCA SCHEDA\n\n");
				nodo.getParent().deleteChild(new Scheda(nomeScheda));
			}
	}
	/**
	 * Crea un file all'interno di una scheda
	 * @param nomeScheda Il nome della scheda in cui aggiungere un file
	 * @param nomeFile Il nome del file da aggiungere
	 * @param dimensione La dimensione del file da aggiungere
	 */

	public void creaFileinScheda(String nomeScheda, String nomeFile, int dimensione)  {
		// cercare scheda, se presente, aggiungere file
		root.getChildren().forEach(each -> {
			if (each.equals(new Node<>(new Scheda (nomeScheda)))) {
				try {
					each.addFile(nomeFile, dimensione);
				} catch (RepositoryException e) {
					e.printStackTrace();
				}
			}
		});		
	}

	/**
	 * Rimuove un file da una scheda
	 * @param nomeScheda Il nome della scheda in cui cercare il file da rimuovere
	 * @param nomeFile Il nome del File da rimuovere
	 */
	public void rimuoviFileinScheda(String nomeScheda, String nomeFile)  {
		// cercare scheda, se presente, aggiungere file
		root.getChildren().forEach(each -> {
			if (each.equals(new Node<>(new Scheda (nomeScheda)))) {
				try {
					each.deleteFile(nomeFile);

				} catch (RepositoryException e) {
					e.printStackTrace();
				}
			}
		});		
	}

	/**
	 * Crea un file nella scheda root del Repository
	 * @param nomeFile Il nome del file da creare
	 * @param dimensione La dimensione del file da creare
	 * @throws RepositoryException
	 */

	public void creaFile(String nomeFile, int dimensione) throws RepositoryException {
		root.addFile(nomeFile, dimensione);
	}

	@Override
	public String toString() {
		return "Repository [nome=" + nome + ", root=" + root.toString() + "]";
	}


	private static <T extends Scheda> void printTree(Node<Scheda> scheda, String appender) {
		System.out.println(appender + scheda.getData());
		scheda.getChildren().forEach(each ->  printTree(each, appender )
				);
	}

	public void printAll() {
		printTree(root, " ");
	}

	public Set<Scheda> getAllSchede(Node<Scheda> node) {
		risultatoAllSchede = new HashSet<Scheda>();
		if (node == null) {
			System.out.println("node is null");
		}
		Set<Node<Scheda>> nodi = node.getChildren();
		Iterator<Node<Scheda>> iteratore = nodi.iterator();
		while (iteratore.hasNext())
			getAllSchede(iteratore.next());
		System.out.printf("%s ", node.getData().toString());
		risultatoAllSchede.add(node.getData());
		System.out.println("numero schede:"+risultatoAllSchede.size());
		return risultatoAllSchede;

	}

	public Set<Scheda> getAllSchedeOrderedBySize() {
		// FIXME deve restituire una copia (usare clonazione)
		Set<Scheda> risultato;
		risultato = new TreeSet<Scheda>(new SchedaByDimensione());
		risultato = getAllSchede(root);

		for (Scheda item: risultato)
			System.out.println("dimensione: "+item.getDimensione());


		return risultato;
	}
}
