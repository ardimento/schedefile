package it.uniba.di.programmazione2.repository.utenza;
import it.uniba.di.programmazione2.repository.Privilegio;

abstract class Utente {

	private String nomeUtente;

	public Utente(String nomeUtente) {
		this.nomeUtente=nomeUtente;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Utente: ");
		if (nomeUtente != null)
			builder.append("nomeUtente=").append(nomeUtente);
		return builder.toString();
	}



}
