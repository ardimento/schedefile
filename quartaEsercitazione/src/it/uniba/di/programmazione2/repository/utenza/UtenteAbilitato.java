package it.uniba.di.programmazione2.repository.utenza;

import it.uniba.di.programmazione2.repository.Privilegio;

/*
 * Rappresenta l'utenza UtenteAbilitato di un Repository
 */
public class UtenteAbilitato extends Utente{
	private Privilegio tipoPrivilegio;

	public UtenteAbilitato(Privilegio tipoPrivilegio, String nomeUtente) {
		super(nomeUtente);
		this.tipoPrivilegio= tipoPrivilegio;
	}
	/**
	 * Permette di modificare il tipo di privilegio
	 * @param tipoPrivilegio
	 */
	public void setPrivilegio(Privilegio tipoPrivilegio) {
		this.tipoPrivilegio=tipoPrivilegio;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("UtenteAbilitato: ");
		if (tipoPrivilegio != null)
			builder.append("tipoPrivilegio=").append(tipoPrivilegio).append(", ");
		if (super.toString() != null)
			builder.append(super.toString());
		return builder.toString();
	}
}