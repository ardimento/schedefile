package it.uniba.di.programmazione2.repository.utenza;

import it.uniba.di.programmazione2.repository.Privilegio;

/**
 * Rappresenta l'utenza Creatore di un Repository
 * @author pasqualeardimento
 *
 */
public class Creatore extends Utente {
	private Privilegio tipoPrivilegio;

	public Creatore(String nomeCreatore) {
		super(nomeCreatore);
		this.tipoPrivilegio =Privilegio.LETTURA_SCRITTURA;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Creatore: ");
		if (tipoPrivilegio != null)
			builder.append("tipoPrivilegio=").append(tipoPrivilegio).append(", ");
		if (super.toString() != null)
			builder.append(super.toString());
		return builder.toString();
	}


}