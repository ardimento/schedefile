package it.uniba.di.programmazione2.repository;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import it.uniba.di.programmazione2.repository.eccezione.CodiceErrore;
import it.uniba.di.programmazione2.repository.eccezione.RepositoryException;
import it.uniba.di.programmazione2.repository.utility.Data;

class File extends AbstractFile{


	public File(String nomeFile, int dimensione) throws RepositoryException {
		super(nomeFile);
		if (checkNome(nomeFile))
			nome = nomeFile;
		else
			throw new RepositoryException(CodiceErrore.NOME_VIOLATION);
		this.dimensione = dimensione;
	}

	public File(String nomeFile) throws RepositoryException {
		super(nomeFile);
		if (checkNome(nomeFile))
			this.nome = nomeFile;
		else
			throw new RepositoryException(CodiceErrore.NOME_VIOLATION);
		this.dimensione = 0;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		File other = (File) obj;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		return true;
	}


	@Override
	public String toString() {
		return "\nFile [nome=" + nome + ", dimensione=" + dimensione + ", dataCreazione=" + dataCreazione
				+ ", dataUltimaModifica=" + dataUltimaModifica + "]";
	}

	private boolean checkNome(String nomeFile) {
		boolean valido = false;

		String regex = "[*.][a-zA-Z]{3}"; 

		// Create a pattern from regex 
		Pattern pattern = Pattern.compile(regex); 
		// Create a matcher for the input String 
		Matcher matcher = pattern.matcher(nomeFile); 
		if (matcher.find())
			valido = true;

		return valido;
	}

	@Override
	public int getDimensione() {
		return this.dimensione;
	}

}
