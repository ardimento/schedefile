package it.uniba.di.programmazione2.repository;

import java.util.Set;

import it.uniba.di.programmazione2.repository.eccezione.CodiceErrore;
import it.uniba.di.programmazione2.repository.eccezione.RepositoryException;
import it.uniba.di.programmazione2.repository.utenza.Creatore;
import it.uniba.di.programmazione2.repository.utenza.UtenteAbilitato;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;


class Scheda extends AbstractFile{
	private Set<File> contenuti;
	private Creatore creatore;
	private Set<UtenteAbilitato> utentiAbilitati;

	private final static int MAX_FILES = 2; 


	public Scheda(String nomeScheda) {
		super(nomeScheda);
		contenuti = new HashSet<File>();
	}

	public int getNumeroElementi() {
		int risultato=0;
		risultato = contenuti.size();
		return risultato;
	}

	public void setCreatore(String nome) {
		this.creatore = new Creatore(nome);

	}

	public void setUtentiAbilitati() {

	}
	/*public void creaScheda(String nomeScheda) {
		contenuti.add(new Scheda(nomeScheda));
	}*/

	public void addFile(String nomeFile, int dimensione) throws RepositoryException {
		if (contenuti.size()<MAX_FILES) {
			contenuti.add(new File(nomeFile, dimensione));
			System.out.println(contenuti.size());
		}
		else
			throw new RepositoryException(CodiceErrore.MAX_NUMBER_EXCEEDED);
	}

	public void deleteFile(String nomeFile) throws RepositoryException {
		for (File item: contenuti) {
			if (item.equals(new File(nomeFile)))
				contenuti.remove(new File(nomeFile));
		}

	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Scheda other = (Scheda) obj;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "\nScheda:"+ this.getNome()+ " [contenuti=" + contenuti + ", creatore=" + creatore + ", utentiAbilitati=" + utentiAbilitati
				+ ", nome=" + nome + ", dimensione=" + dimensione + ", dataCreazione=" + dataCreazione
				+ ", dataUltimaModifica=" + dataUltimaModifica + "]";
	}

	public int getDimensione() {
		int dimensione=0;

		for(File item: contenuti) {
			dimensione+=item.getDimensione();
		}

		dimensione += this.dimensione;
		return dimensione;
	}











}
