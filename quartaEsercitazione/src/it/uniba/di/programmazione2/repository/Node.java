package it.uniba.di.programmazione2.repository;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import it.uniba.di.programmazione2.repository.eccezione.CodiceErrore;
import it.uniba.di.programmazione2.repository.eccezione.RepositoryException;


/**
 * Rappresenta la modalità di organizzare i contenuti di un repository
 * mediante una struttura dati ad albero n-ario composta da:
 * dati, nodo padre e nodo figli
 * 
 * @author pasqualeardimento
 *
 * @param <T> Il tipo parametrico ristretto alla classe Scheda ed alle
 * classi da essa derivata
 */
class Node<T extends Scheda> {

	private final static int MAX_SCHEDE = 2; 

	protected T data = null;
	private Set<Node<T>> children = new HashSet<>();
	private Node<T> parent = null;

	private T risultatoRicerca;

	public Node(T data) {
		this.data = data;
	}

	/**
	 * 
	 * @param size La dimensione rispetto alla quale eseguire il confronto
	 * @return true se la dimensione con la quale operare il confronto è
	 * strettamente inferiore a quella massima consentita; false altrimenti
	 */

	private boolean isOKNumberOfChildren(int size) {
		boolean isOk= false;
		if (size<MAX_SCHEDE)
			isOk= true;
		return isOk;
	}

	public Node<T> addChild(T child) throws RepositoryException {
		if (!isOKNumberOfChildren(children.size())) {
			throw new RepositoryException(CodiceErrore.MAX_NUMBER_EXCEEDED);
		}
		else {
			Node<T> nodo = new Node<T>(child);
			nodo.setParent(this);
			this.children.add(nodo);
			return nodo;
		}
	}

	public boolean deleteChild(T child) throws RepositoryException {
		Node<T> nodo = new Node<T>(child);
		if (this.children.contains(nodo))
			System.out.println("ELEMENTO PRESENTE");
		return this.children.remove(nodo);
	}

	/*	public void addChildren(Set<Node<T>> children) {
		children.forEach(each -> each.setParent(this));
		this.children.addAll(children);
	}*/

	public Set<Node<T>> getChildren() {
		return children;
	}

	public T searchInOrder(T scheda, T schedaToSearch) {
		Node<T> node = new Node<T>(scheda);
		Node<T> nodeToSearch = new Node<T>(schedaToSearch);

		if (node == null) {
			System.out.println("node is null");
		}
		Set<Node<T>> nodi = node.getChildren();
		Iterator<Node<T>> iteratore = nodi.iterator();
		while (iteratore.hasNext())
			searchInOrder(iteratore.next().getData(), nodeToSearch.getData());
		System.out.printf("%s ", node.getData().toString());
		if (node.equals(nodeToSearch))  {
			System.out.printf("TROVATO");
			risultatoRicerca = node.getData();
		}
		return risultatoRicerca;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public void addFile(String nomeFile, int dimensione) throws RepositoryException {
		((Scheda) data).addFile(nomeFile, dimensione);
	}

	public void deleteFile(String nomeFile) throws RepositoryException {
		((Scheda)data).deleteFile(nomeFile);
	}

	private void setParent(Node<T> parent) {
		this.parent = parent;
	}

	public Node<T> getParent() {
		return parent;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Node [");
		if (data != null)
			builder.append("data=").append(data).append(", ");
		if (children != null)
			builder.append("\n\tchildren=").append(children);
		if (parent != null)
			builder.append("\n\tparent=").append(parent.getData());
		builder.append("]");

		return builder.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Node))
			return false;
		Node other = (Node) obj;
		if (data == null) {
			if (other.data != null)
				return false;
		} else if (!data.equals(other.data))
			return false;
		return true;
	}
}