package it.uniba.di.programmazione2.repository;

import java.util.Comparator;

public class SchedaByDimensione implements Comparator<Scheda> {

	@Override
	public int compare(Scheda o1, Scheda o2) {
		return o1.getDimensione()-o2.getDimensione();
	}

}
