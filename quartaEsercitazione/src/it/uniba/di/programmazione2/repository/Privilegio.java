package it.uniba.di.programmazione2.repository;

public enum Privilegio {
	LETTURA, 
	SCRITTURA,
	LETTURA_SCRITTURA
}
