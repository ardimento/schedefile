package it.uniba.di.programmazione2.repository;

import java.util.Date;

import it.uniba.di.programmazione2.repository.utility.Data;

abstract class AbstractFile {
	protected String nome;
	protected int dimensione;
	protected Date dataCreazione;
	protected Date dataUltimaModifica;
	@Override
	public String toString() {
		return "AbstractFile [nome=" + nome + ", dimensione=" + dimensione + ", dataCreazione=" + dataCreazione
				+ ", dataUltimaModifica=" + dataUltimaModifica + "]";
	}
	public AbstractFile(String nome) {
		this.nome = nome;
		this.dimensione = 0;
		this.dataCreazione= Data.getDataAttuale();
		this.dataUltimaModifica= Data.getDataAttuale();
	}

	public String getNome() {
		return nome;
	}

	public abstract int getDimensione();



}
