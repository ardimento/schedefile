package it.uniba.di.programmazione2.test;

import it.uniba.di.programmazione2.repository.Privilegio;
import it.uniba.di.programmazione2.repository.Repository;
import it.uniba.di.programmazione2.repository.eccezione.RepositoryException;
import it.uniba.di.programmazione2.repository.utenza.Creatore;
import it.uniba.di.programmazione2.repository.utenza.UtenteAbilitato;

/**
 * La classe che esegue il test del programma Repository
 * @author pasqualeardimento
 *
 */
public class TestRepository {

	public static void main(String args[]) {

		System.out.println("**** test creazione Repository");
		Repository repo = new Repository("remoto");
		print(repo);

		Creatore creatoreRepo = new Creatore("Master");
		print(creatoreRepo);

		UtenteAbilitato utAbilitato = new UtenteAbilitato(Privilegio.LETTURA_SCRITTURA, "studente");
		print(utAbilitato);

		System.out.println("**** test creazione scheda nella scheda root");
		try {
			repo.creaScheda("primaScheda");
			print(repo);
		} catch (RepositoryException e) {
			System.out.println(e.getMessage());
		}
		try {
			repo.creaScheda("primaScheda");
			print(repo);
		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
		System.out.println("**** test creazione scheda in una scheda NON root");
		try {
			repo.creaSchedainScheda("primaScheda", "SchedaInPrimaScheda");
			print(repo);
		}
		catch (RepositoryException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
		try {
			repo.creaSchedainScheda("SchedaInPrimaScheda", "PrimaSchedaInPrimaScheda");
			print(repo);
		}
		catch (RepositoryException e) {
			System.out.println(e.getMessage());
		}

		try {
			repo.creaFile("primo File.txt", 30);
		} catch (RepositoryException e) {
			System.out.println(e.getMessage());
		}
		try {
			repo.creaFile("secondo File.txt", 60);
		}
		catch (RepositoryException e) {
			System.out.println(e.getMessage());
		}
		try {
			repo.creaFile("terzo File.txt", 70);
		} catch (RepositoryException e) {
			System.out.println(e.getMessage());
		}
		print(repo);

		System.out.println("\n\ntest creazione scheda in scheda presente");
		try {
			repo.creaSchedainScheda("secondaScheda", "schedaCreatainSecondaScheda");
		} catch (RepositoryException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		System.out.println("Aggiunta cartella schedaCreatainSecondaScheda in secondaScheda. "+repo.toString());

		System.out.println("\n\ntest creazione scheda in scheda NON presente");
		try {
			repo.creaSchedainScheda("Scheda", "schedaCreatainScheda");
		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		print(repo);

		System.out.println("\n\ntest creazione file FileNuovo.ext in scheda presente secondaScheda");
		repo.creaFileinScheda("secondaScheda", "FileNuovo.ext", 77);
		print(repo);

		repo.rimuoviFileinScheda("secondaScheda", "FileNuovo.ext");
		System.out.println("\nDopo cancellazione file FileNuovo.ext: \n"+repo.toString());

		System.out.println("\n\ntest creazione file in scheda NON presente");
		repo.creaFileinScheda("Scheda", "FileNuovoScheda.ext", 99);
		print(repo);


		try {
			repo.creaSchedainScheda("primaScheda", "bellissimaScheda");
			repo.creaSchedainScheda("bellissimaScheda", "ultimo livello");
			repo.creaScheda("nomeScheda_ooo");
			print(repo);
		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("\nRIMOZIONE SCHEDE bellissimaScheda e nomeScheda_ooo\n");
		try {
			repo.rimuoviScheda("bellissimaScheda");
			repo.rimuoviScheda("nomeScheda_ooo");
		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("\nLE SCHEDE bellissimaScheda e nomeScheda_ooo non "
				+ "ci dovrebbero essere\n");

		print(repo);

		repo.getAllSchedeOrderedBySize();

	}

	private static void print(Object repo) {
		System.out.println("\n"+repo.toString());
	}

}